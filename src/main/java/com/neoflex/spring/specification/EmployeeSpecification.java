package com.neoflex.spring.specification;

import com.neoflex.spring.dto.EmployeeSearchDto;
import com.neoflex.spring.entities.Employee;
import com.neoflex.spring.entities.Position;
import com.neoflex.spring.entities.PositionDepartment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EmployeeSpecification {

    public static Specification<Employee> applyFilter(EmployeeSearchDto employeeSearchDto) {
        List<Specification<Employee>> specifications = new ArrayList<>();

        if (StringUtils.isNotEmpty(employeeSearchDto.getSurname()))
            specifications.add(filterBySurname(employeeSearchDto.getSurname()));
        if (StringUtils.isNotEmpty(employeeSearchDto.getName()))
            specifications.add(filterByName(employeeSearchDto.getName()));
        if (StringUtils.isNotEmpty(employeeSearchDto.getMiddleName()))
            specifications.add(filterByMiddleName(employeeSearchDto.getMiddleName()));
        if (employeeSearchDto.getSalaryFrom() != null)
            specifications.add(filterBySalaryFrom(employeeSearchDto.getSalaryFrom()));
        if (employeeSearchDto.getSalaryTo() != null)
            specifications.add(filterBySalaryTo(employeeSearchDto.getSalaryTo()));
        if (employeeSearchDto.getHireDateFrom() != null)
            specifications.add(filterByHireDateFrom(employeeSearchDto.getHireDateFrom()));
        if (employeeSearchDto.getHireDateTo() != null)
            specifications.add(filterByHireDateTo(employeeSearchDto.getHireDateTo()));
        if (StringUtils.isNotEmpty(employeeSearchDto.getPosition()))
            specifications.add(filterByPosition(employeeSearchDto.getPosition()));

        if (specifications.size() == 0)
            return null;

        Specification<Employee> linkedSpecifications = specifications.get(0);
        for (int i = 1; i < specifications.size(); i++)
            linkedSpecifications = linkedSpecifications.and(specifications.get(i));

        return linkedSpecifications;
    }

    public static Specification<Employee> filterBySurname(final String surname) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("surname"), surname);
    }

    public static Specification<Employee> filterByName(final String name) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("name"), name);
    }

    public static Specification<Employee> filterByMiddleName(final String middleName) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("middleName"), middleName);
    }

    public static Specification<Employee> filterBySalaryFrom(final BigDecimal salaryFrom) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("salary"), salaryFrom);
    }

    public static Specification<Employee> filterBySalaryTo(final BigDecimal salaryTo) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("salary"), salaryTo);
    }

    public static Specification<Employee> filterByHireDateFrom(final LocalDate hireDateFrom) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("hireDate"), hireDateFrom);
    }

    public static Specification<Employee> filterByHireDateTo(final LocalDate hireDateTo) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("hireDate"), hireDateTo);
    }

    public static Specification<Employee> filterByPosition(final String position) {
        return (Specification<Employee>) (root, criteriaQuery, criteriaBuilder) ->
        {
            Join<PositionDepartment, Employee> joinTableEmpPosDep = root.join("positionDepartment");
            Join<Position, Position> joinTableEmpPosDepPos = joinTableEmpPosDep.join("position");
            return criteriaBuilder.equal(joinTableEmpPosDepPos.get("title"), position);
        };
    }
}
