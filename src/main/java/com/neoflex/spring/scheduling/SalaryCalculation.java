package com.neoflex.spring.scheduling;

import com.neoflex.spring.entities.Employee;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;

public class SalaryCalculation {

    public static BigDecimal compute(Employee emp, LocalDate dateSalaryIssue) {
        BigDecimal salaryInCurrentMonth;
        Period perCurDateHireDate = Period.between(emp.getHireDate(), dateSalaryIssue);

        if (emp.getDateDismissal() != null && Period.between(dateSalaryIssue, emp.getDateDismissal()).getMonths() != 0)
            return null;

        if (emp.getDateDismissal() != null) {
            salaryInCurrentMonth = emp.getSalary().multiply(BigDecimal.valueOf(emp.getDateDismissal().getDayOfMonth()
                    - (emp.getDateDismissal().getMonthValue() == emp.getHireDate().getMonthValue() ? emp.getHireDate().
                    getDayOfMonth() : 0))).divide(BigDecimal.valueOf(emp.getDateDismissal().lengthOfMonth()),
                    2, RoundingMode.HALF_UP);
        } else {
            if (perCurDateHireDate.getMonths() == 0) {
                salaryInCurrentMonth = emp.getSalary().multiply(BigDecimal.valueOf(perCurDateHireDate.getDays())).
                        divide(BigDecimal.valueOf(emp.getHireDate().lengthOfMonth()), 2, RoundingMode.HALF_UP);
            } else {
                salaryInCurrentMonth = emp.getSalary();
            }
        }

        return salaryInCurrentMonth;
    }
}
