package com.neoflex.spring.scheduling;

import com.neoflex.spring.entities.Employee;
import com.neoflex.spring.entities.PayoutHistory;
import com.neoflex.spring.repositories.EmployeeRepository;
import com.neoflex.spring.repositories.PayoutHistoryRepository;
import com.neoflex.spring.services.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class SalaryScheduler {

    private final EmployeeService employeeService;
    private final EmployeeRepository empRep;
    private final PayoutHistoryRepository payHisDep;

    @Transactional
    @Scheduled(cron = "0 0 0 1 * *")
    public void chargeSalariesEmployees() {
        LocalDate currentDate = LocalDate.now();
        for (Employee emp : empRep.findAll()) {
            BigDecimal salaryInCurrentMonth = SalaryCalculation.compute(emp, currentDate);

            if (salaryInCurrentMonth == null)
                continue;

            PayoutHistory ph = employeeService.getLatestPayoutHistory(emp);
            ph.setPaymentDate(currentDate);
            ph.setAmount(salaryInCurrentMonth.subtract(ph.getFine() == null ? BigDecimal.ZERO : ph.getFine()).
                    add(ph.getBonus() == null ? BigDecimal.ZERO : ph.getBonus()));
            payHisDep.save(ph);
        }
    }
}
