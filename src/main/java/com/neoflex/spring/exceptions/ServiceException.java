package com.neoflex.spring.exceptions;

public class ServiceException extends Exception {

    public ServiceException(String message) {
        super(message);
    }
}
