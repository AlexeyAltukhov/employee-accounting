package com.neoflex.spring.exceptions;

public class ControllerException extends Exception {

    public ControllerException(String message) {
        super(message);
    }
}
