package com.neoflex.spring.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Position {

    @Id
    @Column(length = 16)
    private UUID id;

    private String title;

    @OneToMany(mappedBy = "position", cascade = CascadeType.ALL)
    private List<PositionDepartment> positionDepartmentList = new ArrayList<>();
}
