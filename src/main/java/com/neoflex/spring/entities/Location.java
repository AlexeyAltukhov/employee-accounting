package com.neoflex.spring.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    @Id
    @Column(length = 16)
    private UUID id;

    private String country;
    private String region;
    private String city;

    @ManyToMany(mappedBy = "locationList")
    private List<Department> departmentList = new ArrayList<>();
}
