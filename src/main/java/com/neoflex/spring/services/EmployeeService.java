package com.neoflex.spring.services;

import com.neoflex.spring.dto.*;
import com.neoflex.spring.entities.*;
import com.neoflex.spring.exceptions.ServiceException;
import com.neoflex.spring.mappers.MapStructMapper;
import com.neoflex.spring.repositories.*;
import com.neoflex.spring.specification.EmployeeSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
public class EmployeeService {

    private final DepartmentRepository depRep;
    private final LocationRepository locDep;
    private final EmployeeRepository empRep;
    private final PositionRepository posRep;
    private final PositionDepartmentRepository posDepRep;
    private final PayoutHistoryRepository payHisDep;

    private final MapStructMapper map;

    @Transactional
    public void addNewEmployee(EmployeeAddingDto empAddDto) throws ServiceException {
        log.info("Calling method 'addNewEmployee' with parameters {}", empAddDto);
        Department department = depRep.findDepartmentByTitle(empAddDto.getDepartment()).orElseThrow(() ->
                new ServiceException("There is no department with the specified name"));

        Position position = posRep.findPositionByTitle(empAddDto.getPosition()).orElseThrow(() ->
                new ServiceException("There is no position with the specified name"));

        if (locDep.findLocationByCity(empAddDto.getCity()).isEmpty()) {
            throw new ServiceException("There is no city with the specified name");
        }

        if (department.getLocationList().stream().noneMatch((l) -> l.getCity().equals(empAddDto.getCity()))) {
            throw new ServiceException("There is no department with the specified name in the city");
        }

        PositionDepartment positionDepartment = posDepRep.findPositionDepartmentByDepartmentAndPosition(department,
                position).orElseThrow(() -> new ServiceException("Could not find PositionDepartment by position and department"));

        Employee employee = map.employeeAddingDtoToEmployee(empAddDto);
        employee.setPositionDepartment(positionDepartment);
        empRep.save(employee);
    }

    @Transactional
    public void dismissEmployeeById(EmployeeDismissalDto empDisDto) throws ServiceException {
        log.info("Calling method 'dismissEmployeeById' with parameters {}", empDisDto);
        Employee emp = empRep.findById(empDisDto.getId()).orElseThrow(() ->
                new ServiceException("There is no employee with the specified ID"));

        if (empDisDto.getDateDismissal().isBefore(emp.getHireDate())) {
            throw new ServiceException("The date of dismissal must be after the date of hiring");
        }
        if (emp.getDateDismissal() != null) {
            throw new ServiceException("The employee has already been dismissed");
        }

        emp.setDateDismissal(empDisDto.getDateDismissal());
        empRep.save(emp);
    }

    @Transactional
    public List<EmployeeGetDto> getEmployees(EmployeeSearchDto empSearchDto) {
        log.info("Calling method 'getEmployees' with parameters {}", empSearchDto);
        return empRep.findAll(empSearchDto == null ? null : EmployeeSpecification.applyFilter(empSearchDto)).stream()
                .map(this::getEmployeeGetDto).collect(Collectors.toList());
    }

    protected EmployeeGetDto getEmployeeGetDto(Employee emp) {
        log.info("Calling method 'getEmployeeGetDto' with parameters {}", emp);
        EmployeeGetDto empGetDto = map.employeeToEmployeeGetDto(emp);

        PositionDepartment positionDepartment = emp.getPositionDepartment();
        Department department = positionDepartment.getDepartment();
        Position position = positionDepartment.getPosition();

        empGetDto.setCity(department.getLocationList().get(0).getCity());
        empGetDto.setDepartment(department.getTitle());
        empGetDto.setPosition(position.getTitle());
        empGetDto.setTotalPaid(emp.getPayoutHistoryList().stream().map(PayoutHistory::getAmount).
                filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add));

        return empGetDto;
    }

    @Transactional
    public void addNewPayout(PayoutAddingDto payAddDto) throws ServiceException {
        log.info("Calling method 'addNewPayout' with parameters {}", payAddDto);
        Employee emp = empRep.findById(payAddDto.getIdEmployee()).orElseThrow(() ->
                new ServiceException("There is no employee with the specified ID"));

        if (emp.getDateDismissal() != null)
            throw new ServiceException("The employee was fired");

        if (payAddDto.getBonus() == null && payAddDto.getFine() == null)
            throw new ServiceException("The bonus and fine are null");

        PayoutHistory payoutHistory = getLatestPayoutHistory(emp);
        payoutHistory.setPaymentDate(LocalDate.now());
        payoutHistory.setBonus(payoutHistory.getBonus() == null ? payAddDto.getBonus() : payoutHistory.getBonus().
                add(payAddDto.getBonus()));
        payoutHistory.setFine(payoutHistory.getFine() == null ? payAddDto.getFine() : payoutHistory.getFine().
                add(payAddDto.getFine()));

        payHisDep.save(payoutHistory);
    }

    public PayoutHistory getLatestPayoutHistory(Employee emp) {
        log.info("Calling method 'getLatestPayoutHistory' with parameters {}", emp);
        List<PayoutHistory> payoutHistories = emp.getPayoutHistoryList().stream().filter(ph -> ph.getAmount() == null)
                .collect(Collectors.toList());
        if (payoutHistories.size() == 0) {
            PayoutHistory ph = new PayoutHistory();
            ph.setEmployee(emp);
            return ph;
        } else {
            return payoutHistories.get(0);
        }
    }
}
