package com.neoflex.spring.mappers;

import com.neoflex.spring.dto.EmployeeAddingDto;
import com.neoflex.spring.dto.EmployeeGetDto;
import com.neoflex.spring.entities.Employee;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MapStructMapper {

    Employee employeeAddingDtoToEmployee(EmployeeAddingDto employeeAddDTO);

    EmployeeGetDto employeeToEmployeeGetDto(Employee employee);
}
