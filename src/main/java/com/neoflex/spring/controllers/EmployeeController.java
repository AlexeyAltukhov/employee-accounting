package com.neoflex.spring.controllers;

import com.neoflex.spring.controllers.advices.ControllersAdvices;
import com.neoflex.spring.dto.EmployeeAddingDto;
import com.neoflex.spring.dto.EmployeeDismissalDto;
import com.neoflex.spring.dto.EmployeeGetDto;
import com.neoflex.spring.dto.EmployeeSearchDto;
import com.neoflex.spring.exceptions.ControllerException;
import com.neoflex.spring.services.EmployeeService;
import com.neoflex.spring.exceptions.ServiceException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
@Tag(name = "Employee Controller", description = "Controller for working with employees")
public class EmployeeController extends ControllersAdvices {

    private final EmployeeService employeeService;

    @PostMapping("/add")
    @Operation(
            summary = "Employee registration",
            description = "Adding an employee to the system"
    )
    public ResponseEntity<String> addNewEmployee(@RequestBody @Valid EmployeeAddingDto form, BindingResult bindRes)
            throws ControllerException, ServiceException {
        if (form == null) {
            throw new ControllerException("The form for adding an employee is null");
        }
        if (bindRes != null && bindRes.hasErrors()) {
            throw new ControllerException(StringUtils.joinWith("\n", bindRes.getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage).toArray()));
        }
        employeeService.addNewEmployee(form);
        return ResponseEntity.status(HttpStatus.OK).body("Employee added");
    }

    @PostMapping("/dismiss")
    @Operation(
            summary = "Dismissal of an employee",
            description = "Dismissal of an employee by his ID"
    )
    public ResponseEntity<String> dismissEmployeeById(@RequestBody @Valid EmployeeDismissalDto form, BindingResult bindRes)
            throws ControllerException, ServiceException {
        if (form == null) {
            throw new ControllerException("The form of dismissal is null");
        }
        if (bindRes != null && bindRes.hasErrors()) {
            throw new ControllerException(StringUtils.joinWith("\n", bindRes.getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage).toArray()));
        }
        employeeService.dismissEmployeeById(form);
        return ResponseEntity.status(HttpStatus.OK).body("Employee dismissed");
    }

    @GetMapping
    @Operation(
            summary = "Getting a list of employees",
            description = "Getting a list of employees with a specified filtering from the database"
    )
    public List<EmployeeGetDto> getEmployees(@Valid EmployeeSearchDto form, BindingResult bindRes)
            throws ControllerException {
        if (bindRes != null && bindRes.hasErrors()) {
            throw new ControllerException(StringUtils.joinWith("\n", bindRes.getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage).toArray()));
        }
        return employeeService.getEmployees(form);
    }
}
