package com.neoflex.spring.controllers;

import com.neoflex.spring.controllers.advices.ControllersAdvices;
import com.neoflex.spring.dto.PayoutAddingDto;
import com.neoflex.spring.exceptions.ControllerException;
import com.neoflex.spring.exceptions.ServiceException;
import com.neoflex.spring.services.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/payouts")
@RequiredArgsConstructor
@Tag(name = "Payment History Controller", description = "Controller for working with payout histories")
public class PayoutHistoryController extends ControllersAdvices {

    private final EmployeeService employeeService;

    @PostMapping("/add")
    @Operation(
            summary = "Adding a payout",
            description = "Adding a record for issuing a fine or bonus"
    )
    public ResponseEntity<String> addNewPayout(@RequestBody @Valid PayoutAddingDto form, BindingResult bindRes)
            throws ControllerException, ServiceException {
        if (form == null) {
            throw new ControllerException("The form of adding a payout is null");
        }
        if (bindRes != null && bindRes.hasErrors()) {
            throw new ControllerException(StringUtils.joinWith("\n", bindRes.getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage).toArray()));
        }
        employeeService.addNewPayout(form);
        return ResponseEntity.status(HttpStatus.OK).body("Payout added");
    }
}
