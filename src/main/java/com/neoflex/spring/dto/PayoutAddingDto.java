package com.neoflex.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayoutAddingDto {

    @NotNull(message = "Employee ID is null")
    private UUID idEmployee;

    @Min(value = 0, message = "The minimum value of the bonus field should be 0.00")
    private BigDecimal bonus;

    @Min(value = 0, message = "The minimum value of the bonus field should be 0.00")
    private BigDecimal fine;
}
