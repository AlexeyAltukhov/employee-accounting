package com.neoflex.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDismissalDto {

    @NotNull(message = "Employee ID is null")
    private UUID id;

    @NotNull(message = "Date dismissal is null")
    private LocalDate dateDismissal;
}
