package com.neoflex.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeAddingDto {

    @NotNull(message = "Surname is null")
    @Size(min = 1, max = 255, message = "The length of the surname field should be from 1 to 255 characters")
    private String surname;

    @NotNull(message = "Name is null")
    @Size(min = 1, max = 255, message = "The length of the name field should be from 1 to 255 characters")
    private String name;

    @NotNull(message = "Middle name is null")
    @Size(min = 1, max = 255, message = "The length of the middle name field should be from 1 to 255 characters")
    private String middleName;

    @NotNull(message = "Email is null")
    @Size(max = 255, message = "The length of the email field should be no more than 255 characters")
    @Pattern(regexp = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$", message = "Incorrect format of the email field")
    private String email;

    @NotNull(message = "Phone number is null")
    @Size(min = 8, max = 15, message = "The length of the phone number field should be from 8 to 15 characters")
    private String phoneNumber;

    @NotNull(message = "Hire date is null")
    private LocalDate hireDate;

    @NotNull(message = "Salary is null")
    @Min(value = 1, message = "The minimum value of the salary field should be 1.00")
    private BigDecimal salary;

    @NotNull(message = "City is null")
    @Size(min = 1, max = 255, message = "The length of the city field should be from 1 to 255 characters")
    private String city;

    @NotNull(message = "Position is null")
    @Size(min = 1, max = 255, message = "The length of the position field should be from 1 to 255 characters")
    private String position;

    @NotNull(message = "Department is null")
    @Size(min = 1, max = 255, message = "The length of the department field should be from 1 to 255 characters")
    private String department;
}
