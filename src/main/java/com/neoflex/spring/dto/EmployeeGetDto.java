package com.neoflex.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeGetDto {

    private String surname;
    private String name;
    private String middleName;
    private String email;
    private String phoneNumber;
    private LocalDate hireDate;
    private LocalDate dateDismissal;
    private BigDecimal salary;
    private String city;
    private String position;
    private String department;
    private BigDecimal totalPaid;
}
