package com.neoflex.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeSearchDto {

    private String surname;
    private String name;
    private String middleName;

    @Min(value = 1, message = "The minimum value of the salary from field should be 1.00")
    private BigDecimal salaryFrom;

    @Min(value = 1, message = "The minimum value of the salary to field should be 1.00")
    private BigDecimal salaryTo;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate hireDateFrom;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate hireDateTo;
    private String position;
}
