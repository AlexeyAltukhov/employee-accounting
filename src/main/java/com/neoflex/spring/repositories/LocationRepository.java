package com.neoflex.spring.repositories;

import com.neoflex.spring.entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface LocationRepository extends JpaRepository<Location, UUID> {

    Optional<Location> findLocationByCity(String city);
}
