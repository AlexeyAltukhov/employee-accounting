package com.neoflex.spring.repositories;

import com.neoflex.spring.entities.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PositionRepository extends JpaRepository<Position, UUID> {

    Optional<Position> findPositionByTitle(String title);
}
