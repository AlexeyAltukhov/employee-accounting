package com.neoflex.spring.repositories;

import com.neoflex.spring.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface DepartmentRepository extends JpaRepository<Department, UUID> {

    Optional<Department> findDepartmentByTitle(String title);
}
