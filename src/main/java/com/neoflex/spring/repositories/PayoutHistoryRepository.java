package com.neoflex.spring.repositories;

import com.neoflex.spring.entities.PayoutHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PayoutHistoryRepository extends JpaRepository<PayoutHistory, UUID> {
}
