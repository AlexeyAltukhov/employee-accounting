package com.neoflex.spring.repositories;

import com.neoflex.spring.entities.Position;
import com.neoflex.spring.entities.PositionDepartment;
import com.neoflex.spring.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PositionDepartmentRepository extends JpaRepository<PositionDepartment, UUID> {

    Optional<PositionDepartment> findPositionDepartmentByDepartmentAndPosition(Department department, Position position);
}
