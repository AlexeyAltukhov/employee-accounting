CREATE TABLE location (
    id uuid NOT NULL PRIMARY KEY,
    country varchar(255),
    region varchar(255),
    city varchar(255)
);

CREATE TABLE department (
    id uuid NOT NULL PRIMARY KEY,
    title varchar(255)
);

CREATE TABLE department_location (
    department_id uuid,
    location_id uuid,
    FOREIGN KEY (department_id) REFERENCES department (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE position (
    id uuid NOT NULL PRIMARY KEY,
    title varchar(255)
);

CREATE TABLE position_department (
    id uuid NOT NULL PRIMARY KEY,
    position_id uuid,
    department_id uuid,
    FOREIGN KEY (position_id) REFERENCES position (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (department_id) REFERENCES department (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE employee (
    id uuid NOT NULL PRIMARY KEY,
    surname varchar (255),
    name varchar (255),
    middle_name varchar (255),
    email varchar (255),
    phone_number varchar (255),
    hire_date date,
    salary double precision,
    date_dismissal date,
    position_department_id uuid,
    FOREIGN KEY (position_department_id) REFERENCES position_department (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE payout_history (
    id uuid NOT NULL PRIMARY KEY,
    employee_id uuid,
    payment_date date,
    bonus double precision,
    fine double precision,
    amount double precision,
    FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE ON UPDATE CASCADE
);
