package com.neoflex.spring.scheduling;

import com.neoflex.spring.entities.Employee;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class SalaryCalculationTest {

    @Test
    void testSalaryCalculationEmployeeWasFiredMoreThanMonthAgo() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2015, 5, 9));
        emp.setDateDismissal(LocalDate.of(2020, 1, 1));
        LocalDate dateSalaryIssue = LocalDate.of(2020, 6, 1);

        BigDecimal result = SalaryCalculation.compute(emp, dateSalaryIssue);
        assertNull(result);
    }

    @Test
    void testSalaryCalculationEmployeeWasFireLastMonth() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2015, 5, 9));
        emp.setDateDismissal(LocalDate.of(2020, 1, 12));
        emp.setSalary(BigDecimal.valueOf(100000));
        LocalDate dateSalaryIssue = LocalDate.of(2020, 2, 1);

        BigDecimal expected = BigDecimal.valueOf(38709.68);
        BigDecimal result = SalaryCalculation.compute(emp, dateSalaryIssue);
        assertEquals(expected, result);
    }

    @Test
    void testSalaryCalculationEmployeeWasHiredLastMonth() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2020, 1, 9));
        emp.setSalary(BigDecimal.valueOf(100000));
        LocalDate dateSalaryIssue = LocalDate.of(2020, 2, 1);

        BigDecimal expected = BigDecimal.valueOf(74193.55);
        BigDecimal result = SalaryCalculation.compute(emp, dateSalaryIssue);
        assertEquals(expected, result);
    }

    @Test
    void testSalaryCalculationEmployeeWasHiredMoreThanMonthAgo() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2012, 1, 1));
        emp.setSalary(BigDecimal.valueOf(100000));
        LocalDate dateSalaryIssue = LocalDate.of(2020, 2, 1);

        BigDecimal expected = BigDecimal.valueOf(100000);
        BigDecimal result = SalaryCalculation.compute(emp, dateSalaryIssue);
        assertEquals(expected, result);
    }

    @Test
    void testSalaryCalculationEmployeeWasHiredAndFiredLastMonth() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2020, 1, 10));
        emp.setDateDismissal(LocalDate.of(2020, 1, 15));
        emp.setSalary(BigDecimal.valueOf(100000));
        LocalDate dateSalaryIssue = LocalDate.of(2020, 2, 1);

        BigDecimal expected = BigDecimal.valueOf(16129.03);
        BigDecimal result = SalaryCalculation.compute(emp, dateSalaryIssue);
        assertEquals(expected, result);
    }
}