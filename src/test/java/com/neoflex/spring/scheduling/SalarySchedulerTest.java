package com.neoflex.spring.scheduling;

import com.neoflex.spring.entities.Employee;
import com.neoflex.spring.entities.PayoutHistory;
import com.neoflex.spring.repositories.EmployeeRepository;
import com.neoflex.spring.repositories.PayoutHistoryRepository;
import com.neoflex.spring.services.EmployeeService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SalarySchedulerTest {

    private final EmployeeService empService = mock(EmployeeService.class);
    private final EmployeeRepository empRep = mock(EmployeeRepository.class);
    private final PayoutHistoryRepository payHisRep = mock(PayoutHistoryRepository.class);

    private final SalaryScheduler instance = new SalaryScheduler(empService, empRep, payHisRep);

    @Test
    void testChargeSalariesEmployees() {
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.now());
        emp.setSalary(BigDecimal.valueOf(100000));

        when(empRep.findAll()).thenReturn(Collections.singletonList(emp));
        when(empService.getLatestPayoutHistory(emp)).thenReturn(new PayoutHistory());

        instance.chargeSalariesEmployees();

        verify(empRep).findAll();
        verify(empService).getLatestPayoutHistory(any(Employee.class));
        verify(payHisRep).save(any(PayoutHistory.class));
    }
}