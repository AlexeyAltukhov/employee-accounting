package com.neoflex.spring.mappers;

import com.neoflex.spring.dto.EmployeeAddingDto;
import com.neoflex.spring.entities.Employee;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapStructMapperTest {

    private final MapStructMapper map = Mappers.getMapper(MapStructMapper.class);

    @Test
    void testEmployeeMapping() {
        EmployeeAddingDto empPostDTO = new EmployeeAddingDto();
        empPostDTO.setSurname("Алтухов");
        empPostDTO.setName("Алексей");
        empPostDTO.setMiddleName("Николаевич");
        empPostDTO.setEmail("alex@mail.com");
        empPostDTO.setPhoneNumber("88005553535");
        empPostDTO.setHireDate(LocalDate.now());
        empPostDTO.setSalary(BigDecimal.valueOf(80000));

        Employee emp = new Employee();
        emp.setSurname("Алтухов");
        emp.setName("Алексей");
        emp.setMiddleName("Николаевич");
        emp.setEmail("alex@mail.com");
        emp.setPhoneNumber("88005553535");
        emp.setHireDate(LocalDate.now());
        emp.setSalary(BigDecimal.valueOf(80000));

        String expected = emp.toString();
        String result = map.employeeAddingDtoToEmployee(empPostDTO).toString();
        assertEquals(expected, result);
    }
}
