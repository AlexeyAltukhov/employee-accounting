package com.neoflex.spring.services;

import com.neoflex.spring.dto.*;
import com.neoflex.spring.entities.*;
import com.neoflex.spring.exceptions.ServiceException;
import com.neoflex.spring.mappers.MapStructMapper;
import com.neoflex.spring.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private final DepartmentRepository depRep = mock(DepartmentRepository.class);
    private final LocationRepository locDep = mock(LocationRepository.class);
    private final EmployeeRepository empRep = mock(EmployeeRepository.class);
    private final PositionRepository posRep = mock(PositionRepository.class);
    private final PositionDepartmentRepository posDepRep = mock(PositionDepartmentRepository.class);
    private final PayoutHistoryRepository payHisRep = mock(PayoutHistoryRepository.class);
    private final MapStructMapper map = mock(MapStructMapper.class);

    private final EmployeeService instance = new EmployeeService(depRep, locDep, empRep, posRep, posDepRep, payHisRep, map);

    @Test
    void testAddEmployeeNotFoundDepartment() {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewEmployee(empAddDto));

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());

        String expected = "There is no department with the specified name";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddEmployeeNotFoundPosition() {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.of(new Department()));
        when(posRep.findPositionByTitle(empAddDto.getPosition())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewEmployee(empAddDto));

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());
        verify(posRep).findPositionByTitle(empAddDto.getPosition());

        String expected = "There is no position with the specified name";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddEmployeeNotFoundCity() {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.of(new Department()));
        when(posRep.findPositionByTitle(empAddDto.getPosition())).thenReturn(Optional.of(new Position()));
        when(locDep.findLocationByCity(empAddDto.getCity())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewEmployee(empAddDto));

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());
        verify(posRep).findPositionByTitle(empAddDto.getPosition());
        verify(locDep).findLocationByCity(empAddDto.getCity());

        String expected = "There is no city with the specified name";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddEmployeeNotFoundDepartmentInCity() {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.of(new Department()));
        when(posRep.findPositionByTitle(empAddDto.getPosition())).thenReturn(Optional.of(new Position()));
        when(locDep.findLocationByCity(empAddDto.getCity())).thenReturn(Optional.of(new Location()));

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewEmployee(empAddDto));

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());
        verify(posRep).findPositionByTitle(empAddDto.getPosition());
        verify(locDep).findLocationByCity(empAddDto.getCity());

        String expected = "There is no department with the specified name in the city";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddEmployeeNotFoundPositionDepartment() {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();
        empAddDto.setCity("Воронеж");

        Department department = new Department();
        Location location = new Location(null, null, null, "Воронеж", null);
        department.setLocationList(Collections.singletonList(location));

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.of(department));
        when(posRep.findPositionByTitle(empAddDto.getPosition())).thenReturn(Optional.of(new Position()));
        when(locDep.findLocationByCity(empAddDto.getCity())).thenReturn(Optional.of(new Location()));
        when(posDepRep.findPositionDepartmentByDepartmentAndPosition(department, new Position())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewEmployee(empAddDto));

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());
        verify(posRep).findPositionByTitle(empAddDto.getPosition());
        verify(locDep).findLocationByCity(empAddDto.getCity());
        verify(posDepRep).findPositionDepartmentByDepartmentAndPosition(department, new Position());

        String expected = "Could not find PositionDepartment by position and department";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddEmployeeSuccessfulSaving() throws ServiceException {
        EmployeeAddingDto empAddDto = new EmployeeAddingDto();
        empAddDto.setCity("Воронеж");

        Department department = new Department();
        Location location = new Location(null, null, null, "Воронеж", null);
        department.setLocationList(Collections.singletonList(location));

        Employee emp = new Employee();
        emp.setPositionDepartment(new PositionDepartment());

        when(depRep.findDepartmentByTitle(empAddDto.getDepartment())).thenReturn(Optional.of(department));
        when(posRep.findPositionByTitle(empAddDto.getPosition())).thenReturn(Optional.of(new Position()));
        when(locDep.findLocationByCity(empAddDto.getCity())).thenReturn(Optional.of(new Location()));
        when(posDepRep.findPositionDepartmentByDepartmentAndPosition(department, new Position())).
                thenReturn(Optional.of(new PositionDepartment()));
        when(map.employeeAddingDtoToEmployee(empAddDto)).thenReturn(emp);

        instance.addNewEmployee(empAddDto);

        verify(depRep).findDepartmentByTitle(empAddDto.getDepartment());
        verify(posRep).findPositionByTitle(empAddDto.getPosition());
        verify(locDep).findLocationByCity(empAddDto.getCity());
        verify(posDepRep).findPositionDepartmentByDepartmentAndPosition(department, new Position());
        verify(empRep).save(emp);
    }

    @Test
    void testDismissEmployeeNotFoundById() {
        EmployeeDismissalDto empDisDto = new EmployeeDismissalDto();

        when(empRep.findById(empDisDto.getId())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.dismissEmployeeById(empDisDto));

        verify(empRep).findById(empDisDto.getId());

        String expected = "There is no employee with the specified ID";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testDismissEmployeeDateDismissalEarlierDateHiring() {
        EmployeeDismissalDto empDisDto = new EmployeeDismissalDto();
        empDisDto.setDateDismissal(LocalDate.of(2021, 11, 1));
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2021, 11, 15));

        when(empRep.findById(empDisDto.getId())).thenReturn(Optional.of(emp));

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.dismissEmployeeById(empDisDto));

        verify(empRep).findById(empDisDto.getId());

        String expected = "The date of dismissal must be after the date of hiring";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testDismissEmployeeAlreadyFired() {
        EmployeeDismissalDto empDisDto = new EmployeeDismissalDto();
        empDisDto.setDateDismissal(LocalDate.of(2021, 11, 15));
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2020, 1, 1));
        emp.setDateDismissal(LocalDate.of(2020, 12, 30));

        when(empRep.findById(empDisDto.getId())).thenReturn(Optional.of(emp));

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.dismissEmployeeById(empDisDto));

        verify(empRep).findById(empDisDto.getId());

        String expected = "The employee has already been dismissed";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testDismissEmployeeSuccessfulDismissal() throws ServiceException {
        EmployeeDismissalDto empDisDto = new EmployeeDismissalDto();
        empDisDto.setDateDismissal(LocalDate.of(2021, 11, 15));
        Employee emp = new Employee();
        emp.setHireDate(LocalDate.of(2021, 1, 1));

        when(empRep.findById(empDisDto.getId())).thenReturn(Optional.of(emp));

        instance.dismissEmployeeById(empDisDto);

        verify(empRep).findById(empDisDto.getId());
        verify(empRep).save(emp);
    }

    @Test
    void testGetEmployeesNullEmployeeSearchDto() {
        Department dep = new Department(UUID.randomUUID(), null, Collections.singletonList(new Location()), null);
        PositionDepartment posDep = new PositionDepartment(UUID.randomUUID(), new Position(), dep, null);
        Employee emp = new Employee(UUID.randomUUID(), "Алтухов", "Алексей", "Николаевич",
                "a@mail.ru", "89009001010", LocalDate.now(), BigDecimal.valueOf(50000),
                null, posDep, new ArrayList<>());

        when(empRep.findAll((Specification<Employee>) null)).thenReturn(Collections.singletonList(emp));
        when(map.employeeToEmployeeGetDto(any(Employee.class))).thenReturn(new EmployeeGetDto());

        instance.getEmployees(null);

        verify(empRep).findAll((Specification<Employee>) null);
        verify(map).employeeToEmployeeGetDto(emp);
    }

    @Test
    void testGetEmployeesNotNullEmployeeSearchDto() {
        Department dep = new Department(UUID.randomUUID(), null, Collections.singletonList(new Location()), null);
        PositionDepartment posDep = new PositionDepartment(UUID.randomUUID(), new Position(), dep, null);
        Employee emp = new Employee(UUID.randomUUID(), "Алтухов", "Алексей", "Николаевич",
                "a@mail.ru", null, LocalDate.now(), BigDecimal.valueOf(50000),
                null, posDep, new ArrayList<>());

        when(empRep.findAll((Specification<Employee>) null)).thenReturn(Collections.singletonList(emp));
        when(map.employeeToEmployeeGetDto(any(Employee.class))).thenReturn(new EmployeeGetDto());

        instance.getEmployees(new EmployeeSearchDto());

        verify(empRep).findAll((Specification<Employee>) null);
        verify(map).employeeToEmployeeGetDto(emp);
    }

    @Test
    void testGetEmployeeGetDto() {
        Department dep = new Department(UUID.randomUUID(), null, Collections.singletonList(new Location()), null);
        PositionDepartment posDep = new PositionDepartment(UUID.randomUUID(), new Position(), dep, null);
        Employee emp = new Employee(UUID.randomUUID(), "Алтухов", "Алексей", "Николаевич",
                "a@mail.ru", "89009001010", LocalDate.now(), BigDecimal.valueOf(50000),
                null, posDep, new ArrayList<>());

        EmployeeGetDto expected = new EmployeeGetDto("Алтухов", "Алексей", "Николаевич",
                "a@mail.ru", "89009001010", null, null, null,
                "Воронеж", "Программист", "IT отдел", BigDecimal.valueOf(50000));

        when(map.employeeToEmployeeGetDto(any(Employee.class))).thenReturn(expected);
        EmployeeGetDto result = instance.getEmployeeGetDto(emp);

        assertEquals(expected, result);
        verify(map).employeeToEmployeeGetDto(emp);
    }

    @Test
    void testAddNewPayoutNotFoundEmployee() {
        PayoutAddingDto payAddDto = new PayoutAddingDto();

        when(empRep.findById(payAddDto.getIdEmployee())).thenReturn(Optional.empty());

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewPayout(payAddDto));

        verify(empRep).findById(payAddDto.getIdEmployee());

        String expected = "There is no employee with the specified ID";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddNewPayoutEmployeeWasFired() {
        PayoutAddingDto payAddDto = new PayoutAddingDto();
        Employee emp = new Employee();
        emp.setDateDismissal(LocalDate.now());

        when(empRep.findById(payAddDto.getIdEmployee())).thenReturn(Optional.of(emp));

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewPayout(payAddDto));

        verify(empRep).findById(payAddDto.getIdEmployee());

        String expected = "The employee was fired";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddNewPayoutBonusAndFineAreNull() {
        PayoutAddingDto payAddDto = new PayoutAddingDto();
        Employee emp = new Employee();

        when(empRep.findById(payAddDto.getIdEmployee())).thenReturn(Optional.of(emp));

        ServiceException serviceException = assertThrows(ServiceException.class, () ->
                instance.addNewPayout(payAddDto));

        verify(empRep).findById(payAddDto.getIdEmployee());

        String expected = "The bonus and fine are null";
        String result = serviceException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    void testAddNewPayoutSuccessfulSaving() throws ServiceException {
        Employee emp = new Employee();
        PayoutAddingDto payAddDto = new PayoutAddingDto();
        payAddDto.setFine(BigDecimal.valueOf(1000));

        when(empRep.findById(payAddDto.getIdEmployee())).thenReturn(Optional.of(emp));

        instance.addNewPayout(payAddDto);

        verify(empRep).findById(payAddDto.getIdEmployee());
        verify(payHisRep).save(any(PayoutHistory.class));
    }

    @Test
    void testGetLatestPayoutHistoryNoPaymentsPerMonth() {
        Employee emp = new Employee();

        PayoutHistory expected = new PayoutHistory(null, emp, null, null, null, null);
        PayoutHistory result = instance.getLatestPayoutHistory(emp);
        assertEquals(expected, result);
    }

    @Test
    void testGetLatestPayoutHistoryThereIsPaymentInMonth() {
        Employee emp = new Employee();
        PayoutHistory payoutHistory = new PayoutHistory();
        payoutHistory.setEmployee(emp);
        emp.setPayoutHistoryList(Collections.singletonList(payoutHistory));

        PayoutHistory expected = new PayoutHistory(null, emp, null, null, null, null);
        PayoutHistory result = instance.getLatestPayoutHistory(emp);
        assertEquals(expected, result);
    }
}