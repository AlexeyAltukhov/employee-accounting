package com.neoflex.spring.controllers;

import com.neoflex.spring.dto.EmployeeAddingDto;
import com.neoflex.spring.dto.EmployeeDismissalDto;
import com.neoflex.spring.dto.EmployeeGetDto;
import com.neoflex.spring.dto.EmployeeSearchDto;
import com.neoflex.spring.exceptions.ControllerException;
import com.neoflex.spring.exceptions.ServiceException;
import com.neoflex.spring.services.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class EmployeeControllerTest {

    private final EmployeeService employeeService = mock(EmployeeService.class);
    private final EmployeeController instance = new EmployeeController(employeeService);

    @Test
    public void testAddEmployeeCode500() {
        ControllerException controllerException = assertThrows(ControllerException.class, () ->
                instance.addNewEmployee(null, mock(BindingResult.class)));

        String expected = "The form for adding an employee is null";
        String result = controllerException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    public void testAddEmployeeCode200() throws ServiceException, ControllerException {
        ResponseEntity<String> excepted = ResponseEntity.status(HttpStatus.OK).
                body("Employee added");
        EmployeeAddingDto emp = new EmployeeAddingDto();
        ResponseEntity<String> result = instance.addNewEmployee(emp, mock(BindingResult.class));
        verify(employeeService).addNewEmployee(emp);
        assertEquals(excepted, result);
    }

    @Test
    public void testDismissEmployeeCode500() {
        ControllerException controllerException = assertThrows(ControllerException.class, () ->
                instance.dismissEmployeeById(null, mock(BindingResult.class)));

        String expected = "The form of dismissal is null";
        String result = controllerException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    public void testDismissEmployeeCode200() throws ServiceException, ControllerException {
        ResponseEntity<String> excepted = ResponseEntity.status(HttpStatus.OK).
                body("Employee dismissed");
        EmployeeDismissalDto emp = new EmployeeDismissalDto();
        ResponseEntity<String> result = instance.dismissEmployeeById(emp, mock(BindingResult.class));
        verify(employeeService).dismissEmployeeById(emp);
        assertEquals(excepted, result);
    }

    @Test
    public void testGetEmployeesCode500() {
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);

        ControllerException controllerException = assertThrows(ControllerException.class, () ->
                instance.getEmployees(new EmployeeSearchDto(), bindingResult));

        String expected = "";
        String result = controllerException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    public void testGetEmployeesCode200() throws ControllerException {
        EmployeeSearchDto emp = new EmployeeSearchDto();

        List<EmployeeGetDto> expected = new ArrayList<>();
        List<EmployeeGetDto> result = instance.getEmployees(emp, mock(BindingResult.class));

        verify(employeeService).getEmployees(emp);
        assertEquals(expected, result);
    }
}