package com.neoflex.spring.controllers;

import com.neoflex.spring.dto.PayoutAddingDto;
import com.neoflex.spring.exceptions.ControllerException;
import com.neoflex.spring.exceptions.ServiceException;
import com.neoflex.spring.services.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class PayoutHistoryControllerTest {

    private final EmployeeService employeeService = mock(EmployeeService.class);
    private final PayoutHistoryController instance = new PayoutHistoryController(employeeService);

    @Test
    public void testAddPayoutCode500() {
        ControllerException controllerException = assertThrows(ControllerException.class, () ->
                instance.addNewPayout(null, mock(BindingResult.class)));

        String expected = "The form of adding a payout is null";
        String result = controllerException.getMessage();
        assertEquals(expected, result);
    }

    @Test
    public void testAddPayoutCode200() throws ServiceException, ControllerException {
        ResponseEntity<String> excepted = ResponseEntity.status(HttpStatus.OK).
                body("Payout added");
        PayoutAddingDto payAdd = new PayoutAddingDto();
        ResponseEntity<String> result = instance.addNewPayout(payAdd, mock(BindingResult.class));
        verify(employeeService).addNewPayout(payAdd);
        assertEquals(excepted, result);
    }
}