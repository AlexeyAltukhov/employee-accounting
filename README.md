#Project "Accounting and management of employees and salaries"

###Functional
Functionality 
- Adding an employee; 
- Adding a bonus to an employee; 
- Dismissal of an employee; 
- Getting a list of employees with the ability to sort and filter; 
- Receipt of salary for each employee from the date of employment, taking into account the bonus and the last salary 
with the date of dismissal.

###List of technologies
- SpringBoot;
- Java/Kotlin;
- Hibernate; 
- Swagger;
- Git; 
- Spring JPA;
- Flyway; 
- Log4j;
- Lombok.